package com.pernix.gash.pojos;

import org.junit.Assert;
import org.junit.Test;

public class CarPartsTest {
  CarParts carParts = new CarParts(true, true, "5", true, true, "12345", true, true, true, true, true);

  @Test
  public void testCarPartsCreation() {
    Assert.assertNotNull(carParts);
  }

  @Test
  public void testGetAntenna() {
    Assert.assertEquals(true, carParts.getAntenna());
  }

  @Test
  public void testSetAntenna() {
    CarParts carPartsCopy = carParts;
    carPartsCopy.setAntenna(false);
    Assert.assertNotEquals(true, carPartsCopy.getAntenna());
  }

  @Test
  public void testGetLegalDocuments() {
    Assert.assertEquals(true, carParts.getLegal_documents());
  }

  @Test
  public void testSetLegalDocuments() {
    CarParts carPartsCopy = carParts;
    carPartsCopy.setLegal_documents(false);
    Assert.assertNotEquals(true, carPartsCopy.getLegal_documents());
  }

  @Test
  public void testGetEmblems() {
    Assert.assertEquals("5", carParts.getEmblems());
  }

  @Test
  public void testSetEmblems() {
    CarParts carPartsCopy = carParts;
    carPartsCopy.setEmblems("6");
    Assert.assertNotEquals("5", carPartsCopy.getEmblems());
  }

  @Test
  public void testGetTools() {
    Assert.assertEquals(true, carParts.getTools());
  }

  @Test
  public void testSetTools() {
    CarParts carPartsCopy = carParts;
    carPartsCopy.setTools(false);
    Assert.assertNotEquals(true, carPartsCopy.getEmblems());
  }

  @Test
  public void testGetEmergencyKit() {
    Assert.assertEquals(true, carParts.getEmergency_kit());
  }

  @Test
  public void testSetEmergencyKit() {
    CarParts carPartsCopy = carParts;
    carPartsCopy.setEmergency_kit(false);
    Assert.assertNotEquals(true, carPartsCopy.getEmergency_kit());
  }

  @Test
  public void testGetPlates() {
    Assert.assertEquals("12345", carParts.getPlates());
  }

  @Test
  public void testSetPlates() {
    CarParts carPartsCopy = carParts;
    carPartsCopy.setPlates("54321");
    Assert.assertNotEquals(true, carPartsCopy.getPlates());
  }

  @Test
  public void testGetRack() {
    Assert.assertEquals(true, carParts.getRack());
  }

  @Test
  public void testSetRack() {
    CarParts carPartsCopy = carParts;
    carPartsCopy.setRack(false);
    Assert.assertNotEquals(true, carPartsCopy.getPlates());
  }

  @Test
  public void testGetCarpet() {
    Assert.assertEquals(true, carParts.getCarpet());
  }

  @Test
  public void testSetCarpet() {
    CarParts carPartsCopy = carParts;
    carPartsCopy.setCarpet(false);
    Assert.assertNotEquals(true, carPartsCopy.getCarpet());
  }

  @Test
  public void testGetHood() {
    Assert.assertEquals(true, carParts.getHood());
  }

  @Test
  public void testSetHood() {
    CarParts carPartsCopy = carParts;
    carPartsCopy.setHood(false);
    Assert.assertNotEquals(true, carPartsCopy.getHood());
  }

  @Test
  public void testGetTrunk() {
    Assert.assertEquals(true, carParts.getTrunk());
  }

  @Test
  public void testSetTrunk() {
    CarParts carPartsCopy = carParts;
    carPartsCopy.setTrunk(false);
    Assert.assertNotEquals(true, carPartsCopy.getTrunk());
  }

  @Test
  public void testGetHubcaps() {
    Assert.assertEquals(true, carParts.getHubcaps());
  }

  @Test
  public void testSetHubcaps() {
    CarParts carPartsCopy = carParts;
    carPartsCopy.setHubcaps(false);
    Assert.assertNotEquals(true, carPartsCopy.getHubcaps());
  }
}
