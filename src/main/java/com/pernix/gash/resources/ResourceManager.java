package com.pernix.gash.resources;

import java.io.InputStream;

public class ResourceManager {

    public static InputStream getResourceAsInputStream(String resourceName) {
      return ResourceManager.class.getResourceAsStream("/" + resourceName);
    }
}
