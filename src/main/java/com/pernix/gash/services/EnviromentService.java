package com.pernix.gash.services;

public class EnviromentService {

  private static EnviromentService enviromentService = new EnviromentService();

  private EnviromentService() {}

  public static EnviromentService getInstance() {
    return enviromentService;
  }

  public static String getVariable(String name) {
    return System.getenv(name);
  }
}
