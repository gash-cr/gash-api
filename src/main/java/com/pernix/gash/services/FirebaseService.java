package com.pernix.gash.services;

import com.google.gson.Gson;
import com.pernix.gash.pojos.DamagesList;
import com.pernix.gash.pojos.Revision;

import javax.ws.rs.HttpMethod;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class FirebaseService {

  // For local test purposes, use the path "https://gash-cr-staging.firebaseio.com/"
  // "FIREBASE_PATH" is an environment variable instanced in Heroku
  final static String firebase_path = EnviromentService.getVariable("FIREBASE_PATH");

  private static FirebaseService firebaseService = new FirebaseService();

  private FirebaseService() {}

  public static FirebaseService getInstance() {
    return firebaseService;
  }

  public static Revision getRevision(String revisionId) {
    String revisionUrl = firebase_path + "revisions/" + revisionId + ".json";
    HttpURLConnection connection = null;
    Revision revision = new Revision();
    Gson gson = new Gson();
    try {
      URL url = new URL(revisionUrl);
      connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod(HttpMethod.GET);
      BufferedReader in = new BufferedReader(
      new InputStreamReader(connection.getInputStream()));
      String inputLine;
      StringBuffer response = new StringBuffer();
      while ((inputLine = in.readLine()) != null) {
        response.append(inputLine);
      }
      in.close();

      revision = gson.fromJson(response.toString(), Revision.class);
      revision.setDamages(getDamages(revision.getDamagesRef()));
    } catch (IOException e) {
      e.printStackTrace();
    }
    
    return revision;
  }

  public static DamagesList getDamages(String damageRef) {
    DamagesList damages = new DamagesList();
    String revisionUrl = firebase_path + "damages/" + damageRef + ".json";
    HttpURLConnection connection = null;
    Gson gson = new Gson();

    try {
      URL url = new URL(revisionUrl);
      connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod("GET");

      BufferedReader in = new BufferedReader(
        new InputStreamReader(connection.getInputStream()));
      String inputLine;
      StringBuffer response = new StringBuffer();
      while ((inputLine = in.readLine()) != null) {
        response.append(inputLine);
      }
      in.close();

      damages = gson.fromJson(response.toString(), DamagesList.class);

    } catch (IOException e) {
      e.printStackTrace();
    }

    return damages;
  }

}
