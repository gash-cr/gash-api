package com.pernix.gash.factories;

import com.pernix.gash.pojos.Revision;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.StringWriter;

public class HTMLFactory {

    public static String generateHTML(Revision revision, String file){
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty("resource.loader", "class");
        velocityEngine.setProperty("class.resource.loader.class",
          "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

        velocityEngine.init();
        Template template = velocityEngine.getTemplate(file);
        VelocityContext context = new VelocityContext();
        context.put("revision", revision);
        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        return writer.toString();
    }
}
