package com.pernix.gash.pojos;

public class Part {

  private boolean in;
  private String name;
  private boolean out;
  
  public Part(boolean in, String name, boolean out) {
    this.in = in;
    this.name = name;
    this.out = out;
  }

  public boolean isIn() {
 	return in;
  }

  public void setIn(boolean in) {
	this.in = in;
  }

  public String getName() {
	return name;
  }

  public void setName(String name) {
	this.name = name;
  }

  public boolean isOut() {
	return out;
  }

  public void setOut(boolean out) {
	this.out = out;
  }
}
