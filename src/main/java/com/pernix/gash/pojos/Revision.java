package com.pernix.gash.pojos;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.google.gson.annotations.SerializedName;

public class Revision {

  private String timestamp;
  @SerializedName("container_status")
  private String containerStatus;
  @SerializedName("container_ref")
  private String container;
  @SerializedName("container_type")
  private String containterType;
  @SerializedName("seal_no_1")
  private String seal1;
  @SerializedName("seal_no_2")
  private String seal2;
  @SerializedName("seal_no_3")
  private String seal3;
  @SerializedName("seal_no_4")
  private String seal4;
  @SerializedName("seal_no_5")
  private String seal5;
  @SerializedName("seal_no_6")
  private String seal6;
  @SerializedName("user_ref")
  private String user;
  @SerializedName("shipping_co")
  private String shippingCo;
  @SerializedName("shipping_company")
  private String shippingCompany;
  private String size;
  @SerializedName("tracking_id")
  private String guideNumber;
  private String destination;
  @SerializedName("carrier_no")
  private String truckId;
  @SerializedName("driver")
  private String driver;
  private String origin;
  @SerializedName("chassis_type")
  private String chassis;
  @SerializedName("remarks")
  private String observations;
  @SerializedName("tire_seal_1")
  private String tireSeal1;
  @SerializedName("tire_seal_2")
  private String tireSeal2;
  @SerializedName("tire_seal_3")
  private String tireSeal3;
  @SerializedName("tire_seal_4")
  private String tireSeal4;
  @SerializedName("tire_seal_5")
  private String tireSeal5;
  @SerializedName("tire_seal_6")
  private String tireSeal6;
  @SerializedName("damages_ref")
  private String damagesRef;
  private DamagesList damages;
  @SerializedName("parts")
  private Part[] parts;

  public Revision() {
  }

  public Revision(String timestamp, String damagesRef, String containerStatus, String origin, Part[] parts, String container, String containterType, String seal1, String seal2, String seal3, String seal4, String seal5, String seal6, String shippingCompany, String size, String guideNumber, String destination, String truckId, String driver, String chassis, String observations, DamagesList damages, String tireSeal1, String tireSeal2, String tireSeal3, String tireSeal4, String tireSeal5, String tireSeal6, String user) {
    this.timestamp = timestamp;
    this.containerStatus = containerStatus;
    this.container = container;
    this.containterType = containterType;
    this.seal1 = seal1;
    this.seal2 = seal2;
    this.seal3 = seal3;
    this.seal4 = seal4;
    this.seal5 = seal5;
    this.seal6 = seal6;
    this.shippingCompany = shippingCompany;
    this.size = size;
    this.guideNumber = guideNumber;
    this.destination = destination;
    this.truckId = truckId;
    this.driver = driver;
    this.chassis = chassis;
    this.observations = observations;
    this.damages = damages;
    this.tireSeal1 = tireSeal1;
    this.tireSeal2 = tireSeal2;
    this.tireSeal3 = tireSeal3;
    this.tireSeal4 = tireSeal4;
    this.tireSeal5 = tireSeal5;
    this.tireSeal6 = tireSeal6;
    this.user = user;
    this.origin = origin;
    this.parts = parts;
    this.damagesRef = damagesRef;
  }

  public String getDamagesRef() {
    return damagesRef;
  }

  public void setDamagesRef(String damagesRef) {
    this.damagesRef = damagesRef;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public String getTimestamp() throws ParseException {
	Timestamp timestamp = new Timestamp(Long.parseLong(this.timestamp));
	Calendar calendar = GregorianCalendar.getInstance();
	calendar.setTimeInMillis(timestamp.getTime());
    return calendar.getTime().toString();
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public String getTransaction() {
    return containerStatus;
  }

  public void setTransaction(String containerStatus) {
    this.containerStatus = containerStatus;
  }

  public String getContainer() {
    return container;
  }

  public void setContainer(String container) {
    this.container = container;
  }

  public String getContainterType() {
    return containterType;
  }

  public void setContainterType(String containterType) {
    this.containterType = containterType;
  }

  public String getSeal1() {
    return seal1;
  }

  public void setSeal1(String seal1) {
    this.seal1 = seal1;
  }

  public String getSeal2() {
    return seal2;
  }

  public void setSeal2(String seal2) {
    this.seal2 = seal2;
  }

  public String getSeal3() {
    return seal3;
  }

  public void setSeal3(String seal3) {
    this.seal3 = seal3;
  }

  public String getSeal4() {
    return seal4;
  }

  public void setSeal4(String seal4) {
    this.seal4 = seal4;
  }

  public String getSeal5() {
    return seal5;
  }

  public void setSeal5(String seal5) {
    this.seal5 = seal5;
  }

  public String getSeal6() {
    return seal6;
  }

  public void setSeal6(String seal6) {
    this.seal6 = seal6;
  }

  public String getShippingCompany() {
    return shippingCompany;
  }

  public void setShippingCompany(String shippingCompany) {
    this.shippingCompany = shippingCompany;
  }

  public String getSize() {
    return size;
  }

  public void setSize(String size) {
    this.size = size;
  }

  public String getGuideNumber() {
    return guideNumber;
  }

  public void setGuideNumber(String guideNumber) {
    this.guideNumber = guideNumber;
  }

  public String getDestination() {
    return destination;
  }

  public void setDestination(String destination) {
    this.destination = destination;
  }

  public String getTruckId() {
    return truckId;
  }

  public void setTruckId(String truckId) {
    this.truckId = truckId;
  }

  public String getDriver() {
    return driver;
  }

  public void setDriver(String driver) {
    this.driver = driver;
  }

  public String getChassis() {
    return chassis;
  }

  public void setChassis(String chassis) {
    this.chassis = chassis;
  }

  public String getObservations() {
    return observations;
  }

  public void setObservations(String observations) {
    this.observations = observations;
  }

  public DamagesList getDamages() {
    return damages;
  }

  public void setDamages(DamagesList damages) {
    this.damages = damages;
  }

  public String getTireSeal1() {
    return tireSeal1;
  }

  public void setTireSeal1(String tireSeal1) {
    this.tireSeal1 = tireSeal1;
  }

  public String getTireSeal2() {
    return tireSeal2;
  }

  public void setTireSeal2(String tireSeal2) {
    this.tireSeal2 = tireSeal2;
  }

  public String getTireSeal3() {
    return tireSeal3;
  }

  public void setTireSeal3(String tireSeal3) {
    this.tireSeal3 = tireSeal3;
  }

  public String getTireSeal4() {
    return tireSeal4;
  }

  public void setTireSeal4(String tireSeal4) {
    this.tireSeal4 = tireSeal4;
  }

  public String getTireSeal5() {
    return tireSeal5;
  }

  public void setTireSeal5(String tireSeal5) {
    this.tireSeal5 = tireSeal5;
  }

  public String getTireSeal6() {
    return tireSeal6;
  }

  public void setTireSeal6(String tireSeal6) {
    this.tireSeal6 = tireSeal6;
  }

  public String getContainerStatus() {
    return containerStatus;
  }
	
  public void setContainerStatus(String containerStatus) {
    this.containerStatus = containerStatus;
  }
	
  public String getShippingCo() {
    return shippingCo;
  }
	
  public void setShippingCo(String shippingCo) {
    this.shippingCo = shippingCo;
  }

  public Part[] getParts() {
	return parts;
  }

  public void setParts(Part[] parts) {
	this.parts = parts;
  }

}
