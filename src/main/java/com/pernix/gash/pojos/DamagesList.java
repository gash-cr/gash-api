package com.pernix.gash.pojos;

import java.util.ArrayList;

public class DamagesList {

  private ArrayList<Damage> left;
  private ArrayList<Damage> inside;
  private ArrayList<Damage> right;
  private ArrayList<Damage> chassis_front;
  private ArrayList<Damage> chassis_up;
  private ArrayList<Damage> chassis_back;
  private ArrayList<Damage> chassis_side;
  private ArrayList<Damage> front;
  private ArrayList<Damage> roof;
  private ArrayList<Damage> back;
  private ArrayList<Damage> floor;

  public DamagesList() {
  }

  public DamagesList(ArrayList<Damage> left, ArrayList<Damage> inside, ArrayList<Damage> right, ArrayList<Damage> chassis_front, ArrayList<Damage> chassis_up, ArrayList<Damage> chassis_back, ArrayList<Damage> chassis_side, ArrayList<Damage> front, ArrayList<Damage> roof, ArrayList<Damage> back, ArrayList<Damage> floor) {
    this.left = left;
    this.inside = inside;
    this.right = right;
    this.chassis_front = chassis_front;
    this.chassis_up = chassis_up;
    this.chassis_back = chassis_back;
    this.chassis_side = chassis_side;
    this.front = front;
    this.roof = roof;
    this.back = back;
    this.floor = floor;
  }

  public ArrayList<Damage> getLeft() {
    return left;
  }

  public void setLeft(ArrayList<Damage> left) {
    this.left = left;
  }

  public ArrayList<Damage> getInside() {
    return inside;
  }

  public void setInside(ArrayList<Damage> inside) {
    this.inside = inside;
  }

  public ArrayList<Damage> getRight() {
    return right;
  }

  public void setRight(ArrayList<Damage> right) {
    this.right = right;
  }

  public ArrayList<Damage> getChassis_front() {
    return chassis_front;
  }

  public void setChassis_front(ArrayList<Damage> chassis_front) {
    this.chassis_front = chassis_front;
  }

  public ArrayList<Damage> getChassis_up() {
    return chassis_up;
  }

  public void setChassis_up(ArrayList<Damage> chassis_up) {
    this.chassis_up = chassis_up;
  }

  public ArrayList<Damage> getChassis_back() {
    return chassis_back;
  }

  public void setChassis_back(ArrayList<Damage> chassis_back) {
    this.chassis_back = chassis_back;
  }

  public ArrayList<Damage> getChassis_side() {
    return chassis_side;
  }

  public void setChassis_side(ArrayList<Damage> chassis_side) {
    this.chassis_side = chassis_side;
  }

  public ArrayList<Damage> getFront() {
    return front;
  }

  public void setFront(ArrayList<Damage> front) {
    this.front = front;
  }

  public ArrayList<Damage> getRoof() {
    return roof;
  }

  public void setRoof(ArrayList<Damage> roof) {
    this.roof = roof;
  }

  public ArrayList<Damage> getBack() {
    return back;
  }

  public void setBack(ArrayList<Damage> back) {
    this.back = back;
  }

  public ArrayList<Damage> getFloor() {
    return floor;
  }

  public void setFloor(ArrayList<Damage> floor) {
    this.floor = floor;
  }
}
