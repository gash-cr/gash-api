package com.pernix.gash.pojos;

import com.google.gson.annotations.SerializedName;

public class Damage {

  private String name;
  @SerializedName("img_ref")
  private String imageUrl;


  public Damage(String name, String imageUrl) {
    this.name = name;
    this.imageUrl = imageUrl;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
