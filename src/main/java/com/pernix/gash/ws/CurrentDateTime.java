package com.pernix.gash.ws;

import java.sql.Timestamp;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.Path;
import com.pernix.gash.ws.utils.WSConstants;

@Path(WSConstants.VERSION + WSConstants.CURRENT_TIMESTAMP_WS)
public class CurrentDateTime {
	
	@GET
	@Produces("application/json")
	public Response getCurrentDateTime() {
        //method 1
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String json = "{\"timestamp\": \"" + timestamp.getTime() + "\"}";
        return Response.ok(json).build();
	}
	
}
