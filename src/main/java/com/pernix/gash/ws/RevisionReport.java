package com.pernix.gash.ws;

import com.itextpdf.text.DocumentException;
import com.pernix.gash.factories.HTMLFactory;
import com.pernix.gash.factories.PDFFactory;
import com.pernix.gash.pojos.Revision;
import com.pernix.gash.services.FirebaseService;
import com.pernix.gash.ws.utils.WSConstants;

import javax.ws.rs.*;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Response;
import java.io.*;

@Path(WSConstants.VERSION + WSConstants.REVISIONS_WS)
public class RevisionReport {

  @POST
  @Consumes("text/plain")
  @Produces("application/pdf")
  public Response createRevision(String revisionId) throws DocumentException {
    Revision revision = FirebaseService.getRevision(revisionId);
    String REPORT_URL = (revision.getShippingCo().equals("evergreen")) ? "evergreen-report.template.vm" : "gash-report.template.vm";
    String stringReport = HTMLFactory.generateHTML(revision, REPORT_URL);
    Response response = null;

    try {
      ByteArrayOutputStream file = (ByteArrayOutputStream) PDFFactory.createPDF(stringReport);
      CacheControl cc = new CacheControl();
      cc.setMaxAge(0);
      cc.setPrivate(true);

      response = Response
        .ok()
        .type("application/pdf")
        .entity(file.toByteArray())
        .cacheControl(cc)
        .build();
    }catch(IOException | DocumentException e) {
      e.printStackTrace();
    }

    return response;
  }
}
