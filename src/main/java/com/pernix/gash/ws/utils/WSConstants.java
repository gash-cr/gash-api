package com.pernix.gash.ws.utils;


public class WSConstants {

  public static final String VERSION = "api/v1/";
  public static final String REVISIONS_WS = "revisions";
  public static final String CURRENT_TIMESTAMP_WS = "timestamp";
  public static final String PDF_WS = "pdf";
  public static final String STATUS_OK = "Ok";
  public static final String STATUS_FAILED = "Failed";

}
