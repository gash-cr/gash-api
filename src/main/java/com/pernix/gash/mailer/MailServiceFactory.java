package com.pernix.gash.mailer;

import com.pernix.gash.resources.ResourceManager;

import javax.mail.Session;
import java.io.IOException;
import java.util.Properties;

public class MailServiceFactory {
    private static MailService mailService = null;
    static Properties mailServerProperties;

    public static MailService getMailService(){
        if(mailServerProperties == null)
            getMailServiceProperties();
            mailService = (mailService != null) ? mailService : (new MailService(getMailServiceSession(), mailServerProperties));
            return mailService;
        }

    private static Session getMailServiceSession(){
        return Session.getDefaultInstance(mailServerProperties, null);
    }

    private static void getMailServiceProperties(){
        mailServerProperties = new Properties();
        try {
            mailServerProperties.load(ResourceManager.getResourceAsInputStream("mailservice.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
