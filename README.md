#Gash API

### Installation
Heroku and Maven are needed to run the project.

###Setup
To compile and package the application into a WAR
``` Setup mvn clean package ```

###Running tests
```mvn test```

###Running project locally
```mvn clean package jetty:run```

###Deploying to Heroku
To deploy the project to heroku.

####Login with your account
```heroku login ```

####Create Heroku instance
```heroku create```

####Add the files to your repo and commit them
``` git add . ```
``` git commit -m "message" ```

####Push to Heroku
``` git push heroku master ```







